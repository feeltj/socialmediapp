<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //The Post belongs to One User
    public function user()
    {
      return $this->belongsTo('App\User');
    }
    public function likes(){
      return $this->hasMany('App\Like');
    }
}
