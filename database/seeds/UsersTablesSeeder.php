<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
         'first_name' => 'Shifoev',
         'email' => 'benz9494@mail.ru',
         'password' => Hash::make('password')
        ]);
    }
}
