<header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">SocialApp</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                            @auth
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('dashboard')}}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        @endauth
                   </ul>
                   <ul class="navbar-nav">
                       @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ url('/login') }}">Login</a>
                      </li>
                      @endguest
                      @auth
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('account') }}">Account</a>
                      </li>
                      <li class="nav-item"><a href="{{ route('logout')}}" class="nav-link">Logout</a></li>
                      @endauth
                  </ul>
                </div>
              </nav> 
</header>