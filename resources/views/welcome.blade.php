@extends('layouts.master')

@section('content')
<div class="row">
        <div class="col-md-6">
            <h3>Sign Up</h3>
            <form action="{{ route('signup') }}" method="POST">
                <div class="form-group">
                    <label for="email">Your Email</label>
                    <input class="form-control" type="text" name="email" id="email" value="{{ Request::old('email') }}">

                    @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                </div>
                <div class="form-group">
                    <label for="first_name">FirstName</label>
                    <input class="form-control" type="text" name="first_name" id="first_name" value="{{ Request::old('first_name') }}">
                    
                    @if ($errors->has('first_name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                        @endif
                </div>
                <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="password">Your password</label>
                        <input class="form-control" type="password" name="password" id="password" value="{{ Request::old('password') }}"> 

                        @if ($errors->has('password'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                            @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>

        <div class="col-md-6">
                <h3>Sign In</h3>
                <form action="{{ route('signin') }}" method="POST">
                        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email">Your Email</label>
                            <input class="form-control" type="text" name="email" value="{{ Request::old('email') }}">
                        </div>
                        <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label for="password">Your password</label>
                                <input class="form-control" type="password" name="password" value="{{ Request::old('password') }}"> 
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input type="hidden" name="_token" value="{{ Session::token() }}"> 
                </form>
        </div>
</div>
@endsection()